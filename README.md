This repo is meant for mentors' use.

Master branch contains a bunch of files with problems in them.

There's a solution branch that has all the same files with solutions (most of them have
multiple solutions).

I'll be updating this regularly as I sort through more stuff.

PRs welcome, folks. And issues/bug reports/suggestions. Come at me. Brah.

See also:
* [node drills](https://github.com/zacanger/node-drills)
* [and basically all my other repos](https://github.com/zacanger?tab=repositories)
  (there are a lot of examples, boilerplates, demos, and such)

